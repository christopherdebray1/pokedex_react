import { ThemeProvider } from '@emotion/react';
import theme from '../src/modules/emotion/theme';
import { MemoryRouter as Router } from 'react-router-dom';

/** @type { import('@storybook/react').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};


export const decorators = [
  (Story) => (
    <Router>
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    </Router>
  ),
];

export default preview;

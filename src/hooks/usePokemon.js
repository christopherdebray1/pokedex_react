import { useEffect, useState } from "react";
import axios from 'axios';
import { convertSpeciesToPokemonUrl } from "@/utils/utilities";

export default function usePokemon(pokemonIdentifier) {
  const [pokemon, setPokemon] = useState();
  const [pokemonEvolutions, setPokemonEvolutions] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const apiUrl = import.meta.env.VITE_API_URL;

  useEffect(() => {
    async function fetchPokemon() {
      try {
        const pokemonData = await axios.get(`${apiUrl}pokemon/${pokemonIdentifier}`).then((response) => {
          return response.data
        })
        if(!pokemonData) {
          setIsLoading(false);
          return;
        }

        const pokemonSpecieData = await axios.get(pokemonData.species.url).then((response) => response.data);
        let pokemonEvolutionsArray = [];

        pokemonData.species = pokemonSpecieData;
        setPokemon(pokemonData);

        const pokemonEvolutionChain =  await axios.get(pokemonData.species.evolution_chain.url).then((response) => response.data);
        pokemonEvolutionsArray = await getPokemonEvolutions(pokemonEvolutionChain.chain, pokemonData, pokemonEvolutionsArray);
        setPokemonEvolutions(pokemonEvolutionsArray);

        setIsLoading(false);
      } catch (error) {
        console.error('Error : ', error);
        setIsLoading(false);
      }
    }

    async function getPokemonEvolutions(evolutionChain, basePokemon, pokemonEvolutionsArray) {
      try {
        const evolutionUrl = convertSpeciesToPokemonUrl(evolutionChain.species.url);
        const response = await axios.get(convertSpeciesToPokemonUrl(evolutionUrl));
        pokemonEvolutionsArray = [...pokemonEvolutionsArray, response.data];
      
        if (evolutionChain.evolves_to.length) {
          return getPokemonEvolutions(evolutionChain.evolves_to[0], basePokemon, pokemonEvolutionsArray);
        } else {
          return pokemonEvolutionsArray;
        }
      } catch (error) {
        console.error('Error : ', error);
      }
      
    }

    fetchPokemon();
  }, [apiUrl, pokemonIdentifier])

  return {pokemon, pokemonEvolutions, isLoading};
}
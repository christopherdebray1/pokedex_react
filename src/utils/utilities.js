export function ucfirst(word) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

export function formatPokemonId(pokemonId) {
  let pokemonIdString = pokemonId.toString();

  while (pokemonIdString.length < 3) {
    pokemonIdString = '0' + pokemonIdString;
  }

  return pokemonIdString;
}

export function convertSpeciesToPokemonUrl(url) {
  return url.replace("/pokemon-species/", "/pokemon/");
}
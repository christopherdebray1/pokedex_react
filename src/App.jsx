import { Routes } from "@/routes";
import theme from "@/modules/emotion/theme";
import { ThemeProvider as EmotionThemeProvider } from '@emotion/react';
import { ThemeProvider as MuiThemeProvider, createTheme } from '@mui/material/styles';
import AuthProvider from "@/providers/AuthProvider";

const muiTheme = createTheme();

function App() {
  return (
    <EmotionThemeProvider theme={theme}>
      <MuiThemeProvider theme={muiTheme}>
        <AuthProvider>
          <Routes />
        </AuthProvider>
      </MuiThemeProvider>
    </EmotionThemeProvider>
  )
}

export default App

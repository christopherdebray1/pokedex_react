import * as S from "@/styles/pages/BasePage";
import PokemonSearch from "@/components/pokemon/pokemonSearch/PokemonSearch";
import NavMenu from "@/components/utility/navigation/NavMenu";

function Homepage() {
  return <S.BasePage>
      <NavMenu top="0.5rem" right="0.5rem" position="fixed" isDark={false} />
      <PokemonSearch />
  </S.BasePage>
}

export default Homepage;
import * as S from "@/styles/pages/BasePage";
import NavMenu from "@/components/utility/navigation/NavMenu";
import LoginForm from "@/components/user/loginForm";

function Login() {
  return <S.BasePage>
      <NavMenu top="0.5rem" right="0.5rem" position="fixed" isDark={false} />
      <LoginForm />
  </S.BasePage>
}

export default Login;
import * as S from "@/styles/pages/BasePage";
import NavMenu from "@/components/utility/navigation/NavMenu";
import RegisterForm from "@/components/user/registerForm";

function Register() {
  return <S.BasePage>
      <NavMenu top="0.5rem" right="0.5rem" position="fixed" isDark={false} />
      <RegisterForm />
  </S.BasePage>
}

export default Register;
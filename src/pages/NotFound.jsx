import * as S from "@/styles/components/card/BaseCard"
import * as SP from "@/styles/pages/BasePage"
import Arrow from "@/components/utility/Arrow"
import pokeballImg from "@/assets/pokeball.png";
import NavMenu from "@/components/utility/navigation/NavMenu";

function NotFound() {
  return (
    <SP.BasePage>
      <NavMenu top="0.5rem" right="0.5rem" position="fixed" isDark={false} />
      <S.BaseCard>
        <S.BaseCardTitle>
          <S.BaseCardTitleActionLft style={{transform: 'translate(-50%, -25%)'}}>
            <Arrow color="black" direction="left" numberOfArrows={1} routeNavigate={-1} />
          </S.BaseCardTitleActionLft>
          <img src={pokeballImg} alt="A pokeball" />
        </S.BaseCardTitle>
        <S.BaseCardContent>
          This page does not exist
        </S.BaseCardContent>
      </S.BaseCard>
    </SP.BasePage>
  )
}

NotFound.propTypes = {}

export default NotFound

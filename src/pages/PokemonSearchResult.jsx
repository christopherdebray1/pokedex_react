import { useParams } from "react-router-dom";
import usePokemon from "@/hooks/usePokemon";
import * as SP from "@/styles/pages/BasePage";
import * as S from "@/styles/components/card/BaseCard";
import PokemonStats from "@/components/pokemon/pokemonStats/PokemonStats";
import PokemonInfos from "@/components/pokemon/pokemonInfos/PokemonInfos";
import PokemonDisplay from "@/components/pokemon/pokemonDisplay/PokemonDisplay";
import PokemonEvolution from "@/components/pokemon/pokemonEvolution/PokemonEvolution";
import Spinner from "@/components/utility/Spinner";
import NotFound from "@/components/pokemon/pokemonNotFound/PokemonNotFound";
import Arrow from "@/components/utility/Arrow";
import { formatPokemonId, ucfirst } from '@/utils/utilities';
import NavMenu from "@/components/utility/navigation/NavMenu";
import useAuth from "../hooks/useAuth";
import PokemonTeamItem from "../components/pokemon/pokemonTeam/pokemonTeamItem/PokemonTeamItem";

function PokemonSearchResult() {
  const { pokemonIdentifier } = useParams();
  const { pokemon, pokemonEvolutions, isLoading } = usePokemon(pokemonIdentifier);
  const { token } = useAuth();

  return <SP.BasePage>
    {isLoading ? (
      <>
        <NavMenu top="0rem" right="0.5rem" position="fixed" isDark={true} />
        <Spinner />
      </>
    ) : pokemon ? (
      <S.BaseCard padding="0" height="100%">
        <S.BaseCardTitle>
          <S.BaseCardTitleActionLft>
            <Arrow color="black" direction="left" numberOfArrows={1} routeNavigate={-1} />
          </S.BaseCardTitleActionLft>
          {ucfirst(pokemon.name)} #{formatPokemonId(pokemon.id)}
          <NavMenu top="0rem" right="0rem" position="absolute" isDark={true} />
        </S.BaseCardTitle>

        <S.BaseCardContent>
          <PokemonStats stats={pokemon.stats} />

          <PokemonDisplay
            pokemonImg={pokemon.sprites.other['official-artwork'].front_default}
            pokemonName={pokemon.name}
            types={pokemon.types}
          />
          {token && <PokemonTeamItem
            pkmName={pokemon.name}
            imgSrc={pokemon.sprites.other['official-artwork'].front_default}
            pokemonId={pokemon.id}
          />}

          <PokemonInfos
            weight={pokemon.weight}
            height={pokemon.height}
            abilities={pokemon.abilities}
            description={pokemon.species.flavor_text_entries[0].flavor_text}
          />
        </S.BaseCardContent>

        <S.BaseCardFooter>
          {pokemonEvolutions &&
            <PokemonEvolution evolutions={pokemonEvolutions} pokemonColor={pokemon.species.color.name} />
          }
        </S.BaseCardFooter>
      </S.BaseCard>
    ) : (
      <NotFound />
    )}
  </SP.BasePage>
}

export default PokemonSearchResult;
import styled from '@emotion/styled';

export const BasePage = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(${({theme}) => theme.colors.warningLight + ',' + theme.colors.dark });
  width: 100%;
  min-height: 100%;
`;
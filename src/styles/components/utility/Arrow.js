import styled from "@emotion/styled";
import { ReactComponent as ArrowRightSvg } from "@/assets/arrowRight.svg";

export const arrowRotation = {
  top: "270deg",
  right: "0deg",
  left: "180deg",
  bottom: "90deg",
}

export const ArrowContainer = styled.div`
  transform: rotate(${({direction}) => arrowRotation[direction]});
  display: inline-block;
  padding: ${({theme}) => theme.size.xl};
`

export const Arrow = styled(ArrowRightSvg)`
  svg, path {
    fill: ${({color}) => color};
  }
`
import styled from "@emotion/styled";

export const NavMenu = styled.nav`
  position: ${({position}) => position ?? 'relative'};
  top: ${({top}) => top ?? '0px'};
  right: ${({right}) => right ?? '0px'};
  z-index: 100;
  font-size: ${({theme}) => theme.size.md};

  ul {
    padding: 0;
    margin: 0;
    place-items: center space-around;
    display: flex;
  }

  ul li {
    list-style-type: none;
  }

  ul li a, ul li span {
    cursor: pointer;
    text-decoration: none;
    padding: ${({theme}) => theme.size.md + ' ' + theme.size.sm};
    margin: ${({theme}) => theme.size.sm};
    border: ${({theme, isDark}) => isDark ? theme.colors.dark : theme.colors.white} solid 1px;
    border-radius: 9999px;
    color: ${({theme, isDark}) => isDark ? theme.colors.dark : theme.colors.white};
    display: inline-block;
    min-width: 100px;
    text-align: center;
    transition: .3s all;
  }

  ul li a:hover, , ul li span:hover {
    transform: translateY(-${({theme}) => theme.size.xxs});
  }

  @media (max-width: 960px) {
    ul li a, ul li span {
      padding: ${({theme}) => theme.size.sm + ' ' + theme.size.xs};
      margin: 2px;
    }
  }

  @media (max-width: 400px) {
    top: 10px;
    right: 0px;
    transform: translateX(-10%);
  }
`;
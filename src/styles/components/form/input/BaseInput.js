import styled from "@emotion/styled";

export const BaseInput = styled.input`
  padding: ${({theme}) => theme.size.sm + ' ' + theme.size.xs};
  font-size: ${({theme}) => theme.size.lg};
  margin: ${({theme}) => theme.size.xs} 0px;
  display: grid;
  border: none;
  border-radius: ${({theme}) => theme.size.xxs}
`

export const BaseLabel = styled.label`
  color: rgba(0,0,0,.4);
  font-weight: bold;
  font-size: ${({theme}) => theme.size.sm};
`
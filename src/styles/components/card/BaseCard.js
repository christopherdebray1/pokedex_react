import styled from '@emotion/styled'
import { ArrowContainer, Arrow } from '@/styles/components/utility/Arrow'

export const BaseCardTitle = styled.h1`
  margin: 0;
  padding: ${({ theme }) => theme.size.xxl} 0; /* Adjust padding as needed */
  font-size: 24px;
`
export const BaseCardContent = styled.div``
export const BaseCardFooter = styled.footer``
export const BaseCardTitleActionLft = styled.div``
export const BaseCardTitleActionRgt = styled.div``

export const BaseCard = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  background: rgba(255,255,255, .8);
  border-radius: ${({theme}) => theme.size.sm};
  padding: ${({theme, padding}) => padding ?? theme.size.md};
  max-width: 100%;
  height: ${({height}) => height ?? 'auto'};

  ${BaseCardTitle} {
    position: relative;
    width: 100%;
    text-align: center;
    border-bottom: ${({ theme }) => theme.colors.warning} 2px solid;

    ${BaseCardTitleActionLft}, ${BaseCardTitleActionRgt} {
      position: absolute;
      top: 0;
      @media (max-width: 400px) {
        top: 50px;
      }
    }

    ${BaseCardTitleActionLft} {
      left: ${({theme}) => theme.size.xl};
    }

    ${BaseCardTitleActionRgt} {
      right: ${({theme}) => theme.size.xl};
    }

    ${ArrowContainer} {
      cursor: pointer;
      ${Arrow} {
        height:${({theme}) => theme.size.xxl};
      }
    }

    @media (max-width: 400px) {
      padding-top: 80px;
    }
  }

  ${BaseCardContent} {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    padding: ${({theme}) => theme.size.xxl} 0px;

    section {
      margin: ${({theme}) => theme.size.xl} 0px;
    }
  }

  ${BaseCardFooter} {
    width: 100%;
    display: flex;
    justify-content: space-around;
    padding: ${({theme}) => theme.size.xxl} 0px;
  }
`
import styled from "@emotion/styled";

export const PokemonInfos = styled.section`
  padding: ${({theme}) => theme.size.md};
  background: ${({theme}) => theme.colors.white};
  border-radius: ${({theme}) => theme.size.sm};
  box-shadow: 0px 5px 5px black;
  width: 290px;
  height: 265px;
  display: grid;

  p {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    font-weight: bold;
  }
  
  p span:nth-of-type(even) {
    color: ${({theme}) => theme.colors.warning};
  }

  p.description span:nth-of-type(even) {
    width: 100%;
    text-align: center;
  }
`
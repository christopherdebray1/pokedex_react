import styled from "@emotion/styled";

export const PokemonStats = styled.table`
  background-color: ${({theme}) => theme.colors.neutral};
  padding: ${({theme}) => theme.size.xs };
`

export const PokemonStatsCell = styled.td`
  width: 55px;
  height: ${({theme}) => theme.size.sm};
  background-color: ${({theme, isFilled}) => isFilled ? theme.colors.info : theme.colors.white};
`

export const PokemonStatsFooter = styled.tfoot`
  font-weight: bold;
  text-align: center;
`
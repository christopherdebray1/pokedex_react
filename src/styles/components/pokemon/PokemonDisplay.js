import styled from "@emotion/styled";

export const PokemonDisplay = styled.section`
  display: inline-block;
`

export const PokemonImage = styled.img`
  height: ${({pokemonImgSize}) => pokemonImgSize === 'sm' ? '150px' : '285px' };
  background: ${({isPokemonBackgroundActive}) => isPokemonBackgroundActive && 'center no-repeat url("/pokeball.png")'};
`
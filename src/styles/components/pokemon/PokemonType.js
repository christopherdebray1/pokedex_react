import styled from "@emotion/styled";

export const PokemonTypeContainer = styled.div`
  display: flex;
  justify-content: ${({numberOfTypes}) => numberOfTypes === 1 ? 'center' : 'space-between'};
  min-width: 275px;
`

export const PokemonType = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({theme, pokemonType}) => theme.colors.types[pokemonType]};
  border-radius: ${({theme}) => theme.size.xxl};
  color: ${({theme}) => theme.colors.white};
  font-weight: bold;
  text-transform: capitalize;
  box-shadow: 0px 4px 4px 0px #00000040;
  width: 120px;
  height: 60px;
`
import styled from "@emotion/styled";

export const PokemonTeamItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 44px;
  width: 44px;
  background: center no-repeat url("/bg_user_pokemon.svg");
  cursor: pointer;
  transition: transform 0.2s;

  :hover {
    transform: scale(1.2);
  }

  img {
    max-height: 54px;
  }
`;
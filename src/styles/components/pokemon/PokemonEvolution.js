import styled from "@emotion/styled";
import { ArrowContainer, arrowRotation } from "@/styles/components/utility/Arrow";

export const PokemonEvolution = styled.div`
  display: flex;
  align-items: center;
`

export const PokemonEvolutionContainer = styled.section`
  display: flex;
  flex-direction: ${({direction}) => {
    let flexDirection = ['top', 'bottom'].includes(direction) ? 'column' : 'row';
    flexDirection += ['top', 'left'].includes(direction) ? '-reverse' : '';

    return flexDirection;
  }};
  ${PokemonEvolution} {
    flex-direction: ${({direction}) => {
      let flexDirection = ['top', 'bottom'].includes(direction) ? 'column' : 'row';
      flexDirection += ['top', 'left'].includes(direction) ? '-reverse' : '';
  
      return flexDirection;
    }};

    ${ArrowContainer} {
      @media (max-width: 400px) {
        transform: rotate(${() => arrowRotation['bottom']});
      }
    }
  }

  @media (max-width: 400px) {
    flex-direction: column;
    ${PokemonEvolution} {
      flex-direction: column;
    }
  }

  p {
    display: grid;
    text-align: center;
    font-weight: bold;
    font-size: ${({theme}) => theme.size.lg};
  }
`
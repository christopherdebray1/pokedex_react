import styled from "@emotion/styled"

export const BaseButton = styled.button`
  background-color: ${({theme, backgroundColor}) => backgroundColor ?? theme.colors.warningLight };
  border: none;
  padding: ${({theme, padding}) => padding ?? theme.size.md + ' ' + theme.size.xxl };
  cursor: pointer;
  color: ${({textColor}) => textColor ?? 'white'};
  border-radius: 9999px;
  min-height: 64px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`
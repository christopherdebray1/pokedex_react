import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from '@emotion/react';
import theme from '../modules/emotion/theme.js';
import PropTypes from 'prop-types'

const RenderProvider = ({ children }) => {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    </Router>
  )
}

RenderProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export default RenderProvider
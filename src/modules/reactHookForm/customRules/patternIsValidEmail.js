const patternIsValidEmail = {
  value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
  message: 'Invalid email address',
};

export default patternIsValidEmail;
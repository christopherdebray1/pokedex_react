import { render } from '@testing-library/react';
import RenderProvider from '../../providers/RenderProvider';


const customRender = (ui, options) =>
  render(ui, { wrapper: RenderProvider, ...options })

// override render method
export { customRender as render }
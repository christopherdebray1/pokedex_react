import { RouterProvider, createBrowserRouter } from "react-router-dom";
import useAuth from '@/hooks/useAuth';
import routesForPublic from "./routesForPublic";
import routesForNotAuthenticatedOnly from "./routesForNotAuthenticatedOnly";
import routesForAuthenticatedOnly from "./routesForAuthenticatedOnly";

const Routes = () => {
  const { token } = useAuth();

  const router = createBrowserRouter([
    ...routesForPublic,
    ...(!token ? routesForNotAuthenticatedOnly : []),
    ...routesForAuthenticatedOnly,
  ]);

  return <RouterProvider router={router} />;
};

export {
  Routes,
};
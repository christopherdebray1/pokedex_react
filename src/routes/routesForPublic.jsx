import Homepage from "../pages/Homepage";
import PokemonSearchResult from "../pages/PokemonSearchResult";
import NotFound from "../pages/NotFound";

const routesForPublic = [
  {
    path: "/",
    element: <Homepage />,
    label: 'Home',
  },
  {
    path: "/pokemon/search/:pokemonIdentifier",
    element: <PokemonSearchResult />,
    label: 'Pokemon search result',
    ignore: true,
  },
  // {
  //   path: "/storybook",
  //   label: 'Storybook',
  // },
  // {
  //   path: "/api_documentation",
  //   label: 'Api doc',
  // },
  {
    path: "*",
    element: <NotFound />,
    label: 'Not found',
    ignore: true,
  },
];

export default routesForPublic;
import { Navigate, Outlet } from "react-router-dom";
import useAuth from '../hooks/useAuth';

// Is used to avoid user acces to specific route when logged in (like register etc)
function UnAuthRoute() {
  const { token } = useAuth();

  console.log('check unauth', token);
  // Check if the user is authenticated
  if (token) {
    // If authenticated, redirect to the home page
    return <Navigate to="/" />;
  }

  // If not authenticated, render the child routes
  return <Outlet />;
}

export default UnAuthRoute;
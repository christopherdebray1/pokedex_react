import ProtectedRoute from "../routes/ProtectedRoute";

const routesForAuthenticatedOnly = [
  {
    path: "/",
    element: <ProtectedRoute />,
    label: 'Protected Route',
    children: [
      {
        path: "/logout",
        element: <div>Logout</div>,
        label: 'Logout',
        callback: true,
      },
    ],
  },
];

export default routesForAuthenticatedOnly;
import * as S from "@/styles/components/utility/Spinner";

function Spinner() {
  return (
    <S.Spinner />
  )
}

export default Spinner;

import Navlink from "./NavLink";
import PropTypes from 'prop-types';

function NavlinkList({ routes }) {
  const links = [];
  routes.forEach((route) => {
    if (route.children) {
      links.push(<NavlinkList routes={route.children} key={route.label} />);
      return;
    }

    if (route.ignore) {
      return;
    }

    links.push(<Navlink route={route} key={route.label} />);
  });

  return links;
}

NavlinkList.propTypes = {
  routes: PropTypes.array.isRequired
}

export default NavlinkList;
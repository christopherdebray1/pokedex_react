import PropTypes from 'prop-types'
import { Link } from "react-router-dom";
import useAuth from '../../../hooks/useAuth';

function Navlink({ route }) {
  const { setToken } = useAuth();
  const logout = () => {
    setToken(null);
  }

  if (route.callback) {
    return <li><span onClick={logout}>{route.label}</span></li>
  }

  if (!route.element) {
    return <li><a href={route.path}>{route.label}</a></li>
  }

  return <li><Link to={route.path}>{route.label}</Link></li>;
}

Navlink.propTypes = {
  route: PropTypes.object.isRequired
}

export default Navlink;
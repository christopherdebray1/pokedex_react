import useAuth from '../../../hooks/useAuth';
import * as S from "../../../styles/components/utility/navigation/NavMenu";
import PropTypes from 'prop-types';
import routesForPublic from "../../../routes/routesForPublic";
import routesForNotAuthenticatedOnly from "../../../routes/routesForNotAuthenticatedOnly";
import routesForAuthenticatedOnly from "../../../routes/routesForAuthenticatedOnly";
import NavlinkList from "./NavLinkList";

function NavMenu({ right, top, position, isDark = false }) {
  const { token } = useAuth();

  return (
    <S.NavMenu position={position} right={right} top={top} isDark={isDark}>
      <ul>
        <NavlinkList routes={routesForPublic} />
        {
          token ?
          <NavlinkList routes={routesForAuthenticatedOnly} /> :
          <NavlinkList routes={routesForNotAuthenticatedOnly} />
        }
      </ul>
    </S.NavMenu>
  )
}

NavMenu.propTypes = {
  right: PropTypes.string,
  top: PropTypes.string,
  position: PropTypes.string,
  isDark: PropTypes.bool
}

export default NavMenu;

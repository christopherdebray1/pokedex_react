import { render } from '@/modules/testing/customRenderer';
import { screen } from "@testing-library/react";
import Arrow from "./Arrow";
import { beforeEach, describe, it, expect } from 'vitest';

describe('Arrow', () => {
  describe('Display', () => {
    beforeEach(() => {
      render(<Arrow color='blue' />);
    })

    it('Then I expect Arrow to display two arrows by default.', () => {
      const arrows = screen.getAllByRole('svg');
      expect(arrows.length).toBe(2);
    });

    it('Then I expect Arrow to have two be in the right direction by default.', () => {
      const arrowContainer = screen.getByTestId('arrows');
      expect(arrowContainer).toHaveStyle({ transform: 'rotate(0deg)' });
    });
  
    it('Then I expect Arrow to have the color blue.', async () => {
      const arrows = screen.getAllByRole('svg');
      const pathElement = arrows.at(0).querySelector('path');
  
      expect(pathElement).toHaveStyle({ fill: 'blue' });
    });
  })

  describe('When i set the props numberOfArrows to 4 at my component.', () => {
    it('Then I expect Arrow to display four arrows.', async () => {
      render(<Arrow color='blue' numberOfArrows={4} />);
      const arrows = screen.getAllByRole('svg');
      expect(arrows.length).toBe(4);
    });
  })

  describe('When i set the props direction to top at my component.', () => {
    it('Then I expect Arrow to display four arrows.', async () => {
      render(<Arrow direction='top' color='blue' />);
      const arrowContainer = screen.getByTestId('arrows');
      expect(arrowContainer).toHaveStyle({ transform: 'rotate(270deg)' });
    });
  })
});
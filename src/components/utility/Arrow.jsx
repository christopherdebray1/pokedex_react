import PropTypes from 'prop-types'
import * as S from '@/styles/components/utility/Arrow'
import arrowRightSvg from '@/assets/arrowRight.svg'
import { direction } from '../../constants/global';
import { useNavigate } from "react-router-dom";

function Arrow({ color, numberOfArrows = 2, direction = "right", routeNavigate = null }) {
  const arrows = [];
  const navigate = useNavigate();

  for (let arrowCount = 0; arrowCount < numberOfArrows; arrowCount++) {
    arrows.push(
      <S.Arrow
        role="svg"
        key={arrowCount}
        color={color}
        src={arrowRightSvg}
      />
    );
  }

  return (
    <S.ArrowContainer direction={direction} onClick={() => routeNavigate && navigate(routeNavigate) } data-testid="arrows">
      {arrows}
    </S.ArrowContainer>
  )
}

Arrow.propTypes = {
  direction: PropTypes.oneOf(direction),
  numberOfArrows: PropTypes.number,
  color: PropTypes.string.isRequired,
  routeNavigate: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
}

export default Arrow

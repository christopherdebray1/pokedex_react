import Arrow from "@/components/utility/Arrow";

export default {
  title: 'Pokemon/Utility/Arrow',
  component: Arrow,
  tags: ['autodocs'],
  argTypes: {
    color: {
      description: 'The color of the arrows',
    },
    direction: {
      description: 'The direction of the arrows',
    },
    numberOfArrows: {
      description: 'The number of arrow to display',
    },
    routeNavigate: {
      table: {
        disable: true,
      }
    }
  }
}

export const Default = {
  args: {
    color: 'orange'
  }
}

export const DifferentColor = {
  args: {
    color: 'dodgerBlue',
  }
}

export const DirectionTop = {
  args: {
    color: 'blue',
    direction: 'top'
  }
}

export const MultipleArrow = {
  args: {
    color: 'red',
    direction: 'left',
    numberOfArrows: 5
  }
}
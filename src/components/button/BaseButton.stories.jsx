import BaseButton from '@/components/button/BaseButton';
import { theme } from '@/modules/emotion/theme'
import iconRandom from '@/assets/icon_random.png';

export default {
  title: 'Pokemon/Button/BaseButton',
  component: BaseButton,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'centered',
  },
  tags: ['autodocs'],
  argTypes: {
    label: {
      description: 'The button text content, if empty it allows the use of icon instead',
      if: { arg: 'icon', truthy: false },
    },
    backgroundColor: {
      description: 'The button background color, it can be an hex color, rgb or a text'
    },
    textColor: {
      description: 'The button text color'
    },
    padding: {
      description: 'The padding of the button, it must be set directly like a css property for exemple 10px 2px'
    },
    icon: {
      description: 'The icon of the button, if empty it allows the use of label instead',
      options: ['','/src/assets/icon_random.png'],
      control: {
        type: 'select',
        labels: {
          '': 'No icon',
          '/src/assets/icon_random.png': 'With icon',
        }
      },
      if: { arg: 'label', truthy: false },
    },
    handleClick: {
      table: {
        disable: true,
      }
    }
  }
};

export const Default = {
  args: {
    label: 'Button',
  },
};

export const WithIcon = {
  args: {
    icon: iconRandom,
    padding: theme.size.md
  },
};

export const WithCustomColors = {
  args: {
    backgroundColor: 'darkblue',
    textColor: 'white',
    label: 'button'
  },
};

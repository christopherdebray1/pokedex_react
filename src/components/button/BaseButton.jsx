import PropTypes from 'prop-types'
import * as S from '@/styles/components/button/BaseButton';

function BaseButton({ label, backgroundColor, textColor, padding, icon, handleClick }) {
  return (
    <S.BaseButton backgroundColor={backgroundColor} padding={padding} onClick={handleClick} textColor={textColor}>
      {label ?? ''}
      {icon ? <img src={icon} alt="Icon" /> : '' }
    </S.BaseButton>
  )
}

BaseButton.propTypes = {
  label: PropTypes.string,
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,
  padding: PropTypes.string,
  icon: PropTypes.string,
  handleClick: PropTypes.func
}

export default BaseButton;

import PropTypes from 'prop-types'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';
import Rating from '@mui/material/Rating';
import { Divider, Avatar } from '@mui/material';
import { useForm, Controller } from "react-hook-form"
import axios from 'axios';
import { useState } from 'react';
// import patternIsValidEmail from '../../../modules/reactHookForm/customRules/PatternIsValidEmail';

const predefinedImg1 = import.meta.env.VITE_STORAGE_ROOT + '/predefined/user/profile_pictures/default_profile_1.jpeg';
const predefinedImg2 = import.meta.env.VITE_STORAGE_ROOT + '/predefined/user/profile_pictures/default_profile_2.jpeg';

const customIcons = {
  1: {
    icon: <Avatar alt="Predefined image 1" src={predefinedImg1} />,
    label: 'Default image one',
  },
  2: {
    icon: <Avatar alt="Predefined image 2" src={predefinedImg2} />,
    label: 'Default image two',
  },
};

const StyledRating = styled(Rating)(({ theme }) => ({
  '& .MuiRating-icon': {
    marginRight: '10px',
  },
  '& .MuiRating-iconEmpty': {
    color: theme.palette.action.disabled,
    filter: 'grayscale(80%)',
  },
}));

function IconContainer(props) {
  const { value, ...other } = props;
  const icon = <span {...other}>{customIcons[value].icon}</span>;

  return icon;
}

IconContainer.propTypes = {
  value: PropTypes.number.isRequired,
};

function RegisterForm() {
  const apiCustomUrl = import.meta.env.VITE_API_CUSTOM_URL;
  const [infoMessage, setInfoMessage] = useState('');

  const { control, handleSubmit, formState : { errors } } = useForm({
    defaultValues: {
      name: "",
      email: "",
      password: "",
      default_picture: 1,
    },
  })

  const onSubmit = async (data) => {
    const registerRoute = `${apiCustomUrl}users/register`;
    await axios.post(registerRoute, data).then((response) => {
      setInfoMessage('The accound has been registered.');
      return response;
    }).catch((error) => {
      setInfoMessage(error.errors && 'An error as occured');
    });
  }

  return (
    <Card sx={{ minWidth: 350 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Register
        </Typography>
        <Divider inset="none" sx={{ marginBottom: 3 }} />
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box
            sx={{
              '& > :not(style)': { width: "100%", my: 1 },
            }}
          >
            <Controller
              name="name"
              control={control}
              rules={{ required: "You must enter a name." }}
              render={({ field }) => {
                return (
                  <TextField
                    label="Name"
                    helperText={errors.name && errors.name.message}
                    type='text'
                    error={!!errors.name}
                    {...field}
                  />
                );
              }}
            />

            <Controller
              name="email"
              control={control}
              rules={{
                required: "You must enter an email.",
                // pattern: patternIsValidEmail
              }}
              render={({ field }) => {
                return (
                  <TextField
                    label="Email"
                    helperText={errors.email && errors.email.message}
                    error={!!errors.email}
                    {...field}
                  />
                )
              }}
            />

            <Controller
              name="password"
              control={control}
              rules={{
                required: "You must have a password.",
                minLength: {
                  value: 4,
                  message: 'At least 4 caracters long.'
                },
              }}
              render={({ field }) => {
                return (
                  <TextField
                    label="Password"
                    helperText={errors.password && errors.password.message}
                    type='password'
                    error={!!errors.password}
                    {...field}
                  />
                );
              }}
            />
          </Box>

          <Controller
            name="default_picture"
            control={control}
            rules={{ required: "You must select a default picture." }}
            render={({ field }) => {
              return (
                <div>
                  <Typography sx={{ fontSize: 14, marginBottom: 1 }} color="text.secondary">
                    Select a Picture
                  </Typography>
                  <StyledRating
                    name="highlight-selected-only"
                    IconContainerComponent={IconContainer}
                    getLabelText={(value) => customIcons[value].label}
                    highlightSelectedOnly
                    max={Object.keys(customIcons).length}
                    value={(value) => value}
                    {...field}
                    onChange={(event, newValue) => {
                      const parsedValue = newValue !== null ? parseInt(newValue, 10) : null;
                      field.onChange(parsedValue);
                    }}
                  />
                </div>
              )
            }}
          />
          <Button type='submit' sx={{ width: '100%', marginTop: 3 }} variant="contained" size="medium">REGISTER</Button>

          <Typography sx={{ fontSize: 14, marginTop: 3 }} gutterBottom>
            {infoMessage}
          </Typography>
        </form>
      </CardContent>
    </Card>
  )
}

export default RegisterForm

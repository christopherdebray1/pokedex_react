import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Divider } from '@mui/material';
import { useForm, Controller } from "react-hook-form"
import { useState } from 'react';
import axios from 'axios';
import useAuth from '../../../hooks/useAuth';
import { useNavigate } from 'react-router-dom';
// import patternIsValidEmail from '../../../modules/reactHookForm/customRules/PatternIsValidEmail';

function LoginForm() {
  const { setToken } = useAuth();
  const navigate = useNavigate();

  const [infoMessage, setInfoMessage] = useState('');

  const { control, handleSubmit, formState: { errors } } = useForm({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const apiCustomUrl = import.meta.env.VITE_API_CUSTOM_URL;

  const onSubmit = async (data) => {
    const loginRoute = `${apiCustomUrl}users/login`;
    await axios.post(loginRoute, data).then((response) => {
      const token = response.data.token;
      setToken(token);
      navigate("/", { replace: true });
    }).catch((error) => {
      setInfoMessage(error.response.data.errors?.email[0] ?? 'An error as occured');
    });
  }

  return (
    <Card sx={{ minWidth: 350 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Login
        </Typography>
        <Divider inset="none" sx={{ marginBottom: 3 }} />
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box
            sx={{
              '& > :not(style)': { width: "100%", my: 1 },
            }}
          >
            <Controller
              name="email"
              control={control}
              rules={{
                required: "You must enter an email.",
                // pattern: patternIsValidEmail,
              }}
              render={({ field }) => {
                return (
                  <TextField
                    label="Email"
                    helperText={errors.email && errors.email.message}
                    {...field}
                    error={!!errors.email}
                  />
                )
              }}
            />
            <Controller
              name="password"
              control={control}
              rules={{ required: "You must enter a password." }}
              render={({ field }) => {
                return (
                  <TextField
                    label="Password"
                    helperText={errors.password && errors.password.message}
                    type="password"
                    {...field}
                    error={!!errors.password}
                  />
                );
              }}
            />
          </Box>
          <Button type='submit' sx={{ width: '100%', marginTop: 3 }} variant="contained" size="medium">LOGIN</Button>

          <Typography sx={{ fontSize: 14, marginTop: 3 }} color='red' gutterBottom>
            {infoMessage}
          </Typography>
        </form>
      </CardContent>
    </Card>
  )
}

export default LoginForm

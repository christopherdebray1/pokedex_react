import PropTypes from 'prop-types'
import * as S from '@/styles/components/pokemon/PokemonInfos'

function PokemonInfos({abilities, height, weight, description}) {
  return (
    <S.PokemonInfos>
      <p>
        <span>Weight</span>
        <span>{`${weight/10} kg`}</span>
      </p>
      <p>
        <span>Height</span>
        <span>{`${height/10} m`}</span>
      </p>
      <p>
        <span>Abilities</span>
        <span>{abilities.map((ability, index) => `${ability.ability.name}${index < (abilities.length-1) ? ', ' : ''}` )}</span>
      </p>
      <p className='description'>
        <span>Description</span>
        <span>{description}</span>
      </p>
    </S.PokemonInfos>
  )
}

PokemonInfos.propTypes = {
  abilities: PropTypes.array.isRequired,
  height: PropTypes.number.isRequired,
  weight: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired
}

export default PokemonInfos

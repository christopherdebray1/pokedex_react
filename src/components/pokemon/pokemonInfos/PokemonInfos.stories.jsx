import PokemonInfos from "./PokemonInfos"
import { pokemon } from "@/constants/pokemon";

export default {
  title: 'Pokemon/Pokemon/PokemonInfos',
  component: PokemonInfos,
  tags: ['autodocs'],
  argTypes: {
    abilities: {
      description: 'The abilities of the pokemon, an array of object'
    },
    height: {
      description: 'The height of the pokemon, this value will be divided by 10'
    },
    weight: {
      description: 'The weight of the pokemon, this value will be divided by 10'
    },
    description: {
      description: 'The pokemon description'
    }
  }
}

export const Default = {
  args: {
    abilities: pokemon.charmander.abilities,
    height: pokemon.charmander.height,
    weight: pokemon.charmander.weight,
    description: pokemon.charmander.species.flavor_text_entries[0].flavor_text
  }
}
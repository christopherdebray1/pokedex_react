import { render } from '@/modules/testing/customRenderer';
import { screen, getDefaultNormalizer } from "@testing-library/react";
import { beforeEach, describe, it, expect } from "vitest";
import PokemonInfos from "./PokemonInfos";
import { pokemon } from "@/constants/pokemon";

describe('PokemonInfos', () => {
  beforeEach(() => {
    render(<PokemonInfos
      abilities={pokemon.charmander.abilities}
      height={pokemon.charmander.height}
      weight={pokemon.charmander.weight}
      description={pokemon.charmander.species.flavor_text_entries[0].flavor_text}
    />)
  })

  describe('Layout ', () => {
    it('Display the abilites of the pokemon', () => {
      expect(screen.getByText('blaze, solar-power')).toBeInTheDocument();
    })

    it('Display the description of the pokemon', () => {
      expect(screen.getByText(
        pokemon.charmander.species.flavor_text_entries[0].flavor_text, {
          normalizer: getDefaultNormalizer({collapseWhitespace: false})
      })).toBeInTheDocument();
    })

    it('Display the weight of the pokemon converted into kilos', () => {
      const convertedWeightString = `${pokemon.charmander.weight / 10} kg`;
      expect(screen.getByText(convertedWeightString)).toBeInTheDocument();
    })

    it('Display the height of the pokemon converted into meters', () => {
      const convertedHeighttString = `${pokemon.charmander.height / 10} m`;
      expect(screen.getByText(convertedHeighttString)).toBeInTheDocument();
    })
  })
})
import PokemonEvolution from "./PokemonEvolution";
import { evolutions } from "@/constants/pokemon";

export default {
  title: 'Pokemon/Pokemon/PokemonEvolution',
  component: PokemonEvolution,
  tags: ['autodocs'],
  argTypes: {
    evolutions: {
      description: 'The evolutions of the pokemon, an array of object'
    },
    pokemonColor: {
      description: 'The pokemon color, this will define the arrow color'
    },
    direction: {
      description: 'The direction of the evolution group display'
    }
  }
}

export const Default = {
  args: {
    evolutions: evolutions.threeEvolution,
    pokemonColor: 'red'
  }
}

export const TwoEvolution = {
  args: {
    evolutions: evolutions.twoEvolution,
    pokemonColor: 'yellow'
  }
}

export const NoEvolution = {
  args: {
    evolutions: evolutions.noEvolution,
    pokemonColor: 'purple'
  }
}

export const DirectionBottom = {
  args: {
    evolutions: evolutions.threeEvolution,
    pokemonColor: 'red',
    direction: 'bottom'
  }
}
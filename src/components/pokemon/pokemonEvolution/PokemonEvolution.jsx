import PropTypes from 'prop-types'
import * as S from '@/styles/components/pokemon/PokemonDisplay'
import Arrow from '@/components/utility/Arrow'
import * as SE from '@/styles/components/pokemon/PokemonEvolution'
import { formatPokemonId, ucfirst } from '@/utils/utilities'
import { direction } from '../../../constants/global'

function PokemonEvolution({ evolutions, pokemonColor, direction = 'right' }) {
  return (
    <SE.PokemonEvolutionContainer direction={direction}>
      {evolutions.map((pokemon, index) => <SE.PokemonEvolution key={index}>
        <div>
          <S.PokemonImage
            pokemonImgSize="sm"
            isPokemonBackgroundActive={true}
            alt={`Image of ${pokemon.name}`}
            src={pokemon.sprites.other['official-artwork'].front_default}
          />
          <p>
            <span>#{formatPokemonId(pokemon.id)}</span>
            <span>{ucfirst(pokemon.name)}</span>
          </p>
        </div>
          {index < (evolutions.length-1) && <Arrow color={pokemonColor} direction={direction} />}
      </SE.PokemonEvolution>
      )}
    </SE.PokemonEvolutionContainer>
  )
}

PokemonEvolution.propTypes = {
  evolutions: PropTypes.array.isRequired,
  pokemonColor: PropTypes.string.isRequired,
  direction: PropTypes.oneOf(direction)
}

export default PokemonEvolution

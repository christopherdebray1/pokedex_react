import { useParams } from "react-router-dom";
import * as S from "@/styles/components/card/BaseCard";
import Arrow from "@/components/utility/Arrow";

function NotFound() {
  const { pokemonIdentifier } = useParams();

  return (
    <S.BaseCard>
      <S.BaseCardTitle>
        <S.BaseCardTitleActionLft style={{transform: 'translate(-50%, -25%)'}}>
          <Arrow color="black" direction="left" numberOfArrows={1} routeNavigate={-1} />
        </S.BaseCardTitleActionLft>
        <img src="@/public/pokeball.png" alt="A pokeball" />
      </S.BaseCardTitle>
      <S.BaseCardContent>
        The pokemon with the identifier or name : {pokemonIdentifier ?? ""} does not exist.
      </S.BaseCardContent>
    </S.BaseCard>
  )
}


export default NotFound;

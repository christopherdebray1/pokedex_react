import { render } from '@/modules/testing/customRenderer';
import { screen } from "@testing-library/react";
import { userEvent } from '@testing-library/user-event';
import { beforeEach, describe, expect, it } from "vitest";
import PokemonSearch from "./PokemonSearch";

describe('PokemonSearch', () => {
  beforeEach(() => {
    render(<PokemonSearch />);
  })

  describe('Layout', () => {
    it('Display the label "POKEMON NAME OR IT S ID"', () => {
      expect(screen.getByRole('label')).toBeInTheDocument();
    })

    it('Display an input', () => {
      expect(screen.getByRole('textbox')).toBeInTheDocument();
    })

    it('Display two buttons', () => {
      expect(screen.getAllByRole('button').length).toBe(2);
    })
  })

  describe('Interactions', () => {
    it('Display error message when you try to send form with empty input', async () => {
      const formButtons = screen.getAllByRole('button');
      await userEvent.click(formButtons[0]);
      expect(screen.getByText('This field is required.'));
    })

    it('Display error message when you try to send the form with a value with a length greater than 50', async () => {
      const formButtons = screen.getAllByRole('button');
      const input = screen.getByRole('textbox');
      await userEvent.type(input, 'morethan50morethan50morethan50morethan50morethan50.');
      await userEvent.click(formButtons[0]);
      expect(screen.getByText('Maximum 50 characters.'));
    })

    it('Go to the search result route with a random number', async () => {
      const formButtons = screen.getAllByRole('button');
      await userEvent.click(formButtons[1]);
      const isPathPokemonId = /^\d+$/.test(window.location.pathname.split('/').at(-1));
      expect(isPathPokemonId).toBe(true);
    })

    it('Go to the search result with the text inputed in the search input', async () => {
      const input = screen.getByRole('textbox');
      const formButtons = screen.getAllByRole('button');
      await userEvent.type(input, 'Charizard');
      await userEvent.click(formButtons[0]);
      expect(window.location.pathname.split('/').at(-1)).toBe('charizard');
    })
  })
})
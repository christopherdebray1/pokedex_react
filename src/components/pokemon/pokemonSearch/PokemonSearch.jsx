import * as S from "@/styles/components/card/BaseCard";
import * as SI from '@/styles/components/form/input/BaseInput';
import BaseButton from "@/components/button/BaseButton";
import { useForm } from "react-hook-form";
import { useTheme } from "@emotion/react";
import { useNavigate } from 'react-router-dom';
import { ErrorMessage } from "@hookform/error-message"
import iconRandom from '@/assets/icon_random.png'

function PokemonSearch() {
  const {
    register,
    formState: { errors },
    handleSubmit
  } = useForm();
  const theme = useTheme();
  const navigate = useNavigate();

  const onSubmit = (data) => {
    navigate(`/pokemon/search/${data.pokemonSearch.toLowerCase()}`);
  };

  const searchRandomPokemon = (e) => {
    e.preventDefault();
    const randomPokemonId = Math.floor(Math.random() * (999 - 1) + 1);
    navigate(`/pokemon/search/${randomPokemonId}`);
  }

  return (
    <S.BaseCard>
      <form onSubmit={handleSubmit(onSubmit)}>
        <SI.BaseLabel role="label" htmlFor="pokemonSearch">
          POKEMON NAME OR IT S ID
          <SI.BaseInput 
            name="pokemonSearch"
            {...register('pokemonSearch', {
              required: 'This field is required.',
              maxLength: {
                value: 50,
                message: 'Maximum 50 characters.'
              }
            })}
          />
          <ErrorMessage errors={errors} name={'pokemonSearch'} render={({ message }) => <p style={{ color: "red", fontWeight: 'normal' }}>{message}</p>} />
        </SI.BaseLabel>
        <div
          style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: theme.size.md}}
        >
          <BaseButton label="Search !" />
          <BaseButton icon={iconRandom} padding={theme.size.md} handleClick={searchRandomPokemon} />
        </div>
      </form>
    </S.BaseCard>
  )
}

export default PokemonSearch

import { render } from '@/modules/testing/customRenderer';
import { screen } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import PokemonType from "./PokemonType";
import theme from "@/modules/emotion/theme";

describe('PokemonType', () => {
  const themeArray = Object.entries(theme.colors.types);
  const getTypeAsObject = (selectedType) => {
    return [{
      type: {
        name: selectedType[0],
        color: selectedType[1]
      }
    }];
  };

  describe('Layout', () => {
    it.each(themeArray)('Display the component PokemonType with the name "%s".', (typeName, typeColor) => {
      render(<PokemonType types={getTypeAsObject([typeName, typeColor])} />);
      expect(screen.getByText(typeName)).toBeInTheDocument();
    })

    it.each(themeArray)('Display the component PokemonType having the name "%s" with the color "%s".', (typeName, typeColor) => {
      render(<PokemonType types={getTypeAsObject([typeName, typeColor])} />);

      const pokemonType = screen.getByText(typeName);
      expect(pokemonType).toHaveStyle({ backgroundColor: typeColor });
    })
  })
})
import PokemonType from "./PokemonType";
import { pokemon } from "@/constants/pokemon";

export default {
  title: "Pokemon/Pokemon/PokemonType",
  component: PokemonType,
  tags: ['autodocs'],
  argTypes: {
    types: {
      description: 'The types is an array containing one or two object, each object will contain nested object, type within you will find the name key indicating type color',
      options: [pokemon.charmander.types, pokemon.venusaur.types],
      control: {
        type: 'radio',
        labels: {
          [pokemon.charmander.types]: 'Fire',
          [pokemon.venusaur.types]: 'Grass and Poison'
        }
      },
    },
  }
}

export const Default = (args) => (
  <div style={{ display: 'flex' }}>
    <PokemonType {...args} />
  </div>
);

Default.args = {
  types: pokemon.charmander.types,
};

export const TwoTypes = (args) => (
  <div style={{ display: 'flex' }}>
    <PokemonType {...args} />
  </div>
);

TwoTypes.args = {
  types: pokemon.venusaur.types
};
import PropTypes from 'prop-types'
import * as S from '@/styles/components/pokemon/PokemonType'

function PokemonType({ types }) {
  return (
    <S.PokemonTypeContainer numberOfTypes={types.length}>
      {types.map((type, index) => <S.PokemonType key={index} role='type' pokemonType={type.type.name}>{type.type.name}</S.PokemonType>)}
    </S.PokemonTypeContainer>
  )
}

PokemonType.propTypes = {
  types: PropTypes.array.isRequired
}

export default PokemonType

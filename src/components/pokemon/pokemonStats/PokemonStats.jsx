import PropTypes from 'prop-types'
import * as S from '@/styles/components/pokemon/PokemonStats';

function PokemonStats({ stats, statRowsCount = 15, maxStat = 255 }) {
  const statByRow = maxStat / statRowsCount;

  const statRows = [];
  for (let statByRowMultiplier = 1; statByRowMultiplier <= statRowsCount; statByRowMultiplier++) {
    statRows.push(
      <tr key={statByRowMultiplier}>
        {stats.map((stat, index) => <S.PokemonStatsCell key={`${statRowsCount}_${index}`} isFilled={ stat.base_stat >= (statByRow * statByRowMultiplier) } />)}
      </tr>
    );
  }
  
  return (
    <section>
      <S.PokemonStats>
        <tbody>
          {statRows.reverse()}
        </tbody>

        <S.PokemonStatsFooter>
          <tr>
            {stats.map((stat, index) => <td key={index}>{stat.stat.name}</td>)}
          </tr>
        </S.PokemonStatsFooter>
      </S.PokemonStats>
    </section>
  )
}

PokemonStats.propTypes = {
  stats: PropTypes.array.isRequired,
  statRowsCount: PropTypes.number,
  maxStat: PropTypes.number
}

export default PokemonStats

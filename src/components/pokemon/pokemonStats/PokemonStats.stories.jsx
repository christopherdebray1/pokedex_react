import PokemonStats from "./PokemonStats";
import { pokemon } from "@/constants/pokemon"

export default {
  title: 'Pokemon/Pokemon/PokemonStats',
  component: PokemonStats,
  description: 'This component is using the statRowsCount to determine the number of rows, \
    it uses the maxStat to define the number of rows to fill depending on the pokemon stats value. \
    At default, 15 is the number of rows and 255 the max stat value, so to fill 4 row the stat of the pokemon must be \
    at least of 68 (statRowsCount / maxStat) = 17, 17 * 4 = 68',
  argTypes: {
    stats: {
      description: 'An array of object indicating each of the pokemon stats'
    },
    statRowsCount: {
      description: 'Indicates the number of rows for each stat'
    },
    maxStat: {
      description: 'Indicates the maximum value of a stat foreach row'
    }
  }
}

export const Default = {
  args: {
    stats: pokemon.venusaur.stats,
  }
}

export const WithCustomStatRowsAndMax = {
  args: {
    stats: pokemon.venusaur.stats,
    statRowsCount: 8,
    maxStat: 120
  }
}
import PropTypes from 'prop-types'
import * as PTI from '../../../../styles/components/pokemon/pokemonTeam/pokemonTeamItem/PokemonTeamItem';

function PokemonTeamItem({ imgSrc, pkmName, pokemonId, hasPokemon = false }) {
  const savePokemon = () => {
    const pokemonData = {
      "name": pkmName,
      "pokedex_id": pokemonId,
      "picture_url": imgSrc
    };
    console.log(pokemonData);
  }

  return (
    <PTI.PokemonTeamItem onClick={savePokemon}>
      {hasPokemon && imgSrc && <img src={imgSrc} alt={pkmName} />}
    </PTI.PokemonTeamItem>
  )
}

PokemonTeamItem.propTypes = {
  imgSrc: PropTypes.string,
  pkmName: PropTypes.string,
  pokemonId: PropTypes.number,
  hasPokemon: PropTypes.bool
}

export default PokemonTeamItem

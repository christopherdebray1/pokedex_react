import { describe, it, expect } from "vitest";
import { render } from '@/modules/testing/customRenderer';
import { screen } from "@testing-library/react";
import PokemonDisplay from "./PokemonDisplay";
import { pokemon } from "@/constants/pokemon";

describe('PokemonDisplay', () => {

  describe('Layout', () => {
    const setupCharizard = () => {
      render(<PokemonDisplay
        types={pokemon.charmander.types}
        pokemonImg={pokemon.charmander.sprites['official-artwork'].front_default}
        pokemonName={pokemon.charmander.name}
      />)
    }

    const setupVenusaur = () => {
      render(<PokemonDisplay
        types={pokemon.venusaur.types}
        pokemonImg={pokemon.venusaur.sprites['official-artwork'].front_default}
        pokemonName={pokemon.venusaur.name}
      />)
    }

    it('Display the pokemon image.', async () => {
      setupCharizard();
      expect(screen.getByRole('image')).toBeInTheDocument();
    })

    it('Pokemon image as the correct url.', async () => {
      setupCharizard();
      expect(screen.getByRole('image').src).toBe(pokemon.charmander.sprites['official-artwork'].front_default);
    })

    it('Image has the correct alt attribute.', () => {
      setupCharizard();
      expect(screen.getByAltText(`Image of ${pokemon.charmander.name}`)).toBeInTheDocument();
    })

    it('Display one pokemon type if the pokemon has one type', () => {
      setupCharizard();
      const types = screen.getAllByRole('type');
      expect(types.length).toBe(1);
    })

    it('Display the correct pokemon type name if the pokemon has one type', () => {
      setupCharizard();
      expect(screen.getByText('fire')).toBeInTheDocument();
    })

    it('Display two pokemon types if the pokemon has two types', () => {
      setupVenusaur();
      const types = screen.getAllByRole('type');
      expect(types.length).toBe(2);
    })

    it('Display the correct pokemon type names if the pokemon has two type', () => {
      setupVenusaur();
      expect(screen.getByText('poison')).toBeInTheDocument();
      expect(screen.getByText('grass')).toBeInTheDocument();
    })
  })
})
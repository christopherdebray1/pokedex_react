import PokemonDisplay from "./PokemonDisplay";
import { pokemon } from "@/constants/pokemon";

export default {
  title: 'Pokemon/Pokemon/PokemonDisplay',
  component: PokemonDisplay,
  tags: ['autodocs'],
  argTypes: {
    types: {
      description: 'The types is an array containing one or two object, each object will contain nested object, type within you will find the name key indicating type color',
      options: [pokemon.charmander.types, pokemon.venusaur.types],
      control: {
        type: 'radio',
        labels: {
          [pokemon.charmander.types]: 'Fire',
          [pokemon.venusaur.types]: 'Grass and Poison'
        }
      },
    },
    pokemonImg: {
      options: [
        pokemon.charmander.sprites["official-artwork"].front_default,
        pokemon.venusaur.sprites["official-artwork"].front_default
      ],
      description: 'The pokemon image to display',
      control: {
        type: 'radio',
        labels: {
          [pokemon.charmander.sprites["official-artwork"].front_default]: 'Charmander artwork',
          [pokemon.venusaur.sprites["official-artwork"].front_default]: 'Venusaur artwork',
        }
      },
    },
    pokemonName: {
      description: 'The pokemon name, displayed only when the image can\'t be loaded',
      options: [pokemon.charmander.name, pokemon.venusaur.name],
      control: { type: 'radio' },
    },
  }
}

export const Default = {
  args: {
    types: pokemon.charmander.types,
    pokemonImg: pokemon.charmander.sprites["official-artwork"].front_default,
    pokemonName: pokemon.charmander.name
  }
}

export const TwoTypePokemon = {
  args: {
    types: pokemon.venusaur.types,
    pokemonImg: pokemon.venusaur.sprites["official-artwork"].front_default,
    pokemonName: pokemon.venusaur.name
  }
}
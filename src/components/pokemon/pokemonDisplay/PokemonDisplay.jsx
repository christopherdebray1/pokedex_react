import PropTypes from 'prop-types';
import PokemonType from '@/components/pokemon/pokemonType/PokemonType';
import * as S from '@/styles/components/pokemon/PokemonDisplay';

function PokemonDisplay({ types, pokemonImg, pokemonName }) {
  return (
    <S.PokemonDisplay>
      <S.PokemonImage role='image' src={pokemonImg} alt={`Image of ${pokemonName}`} />
      <PokemonType types={types} />
    </S.PokemonDisplay>
  )
}

PokemonDisplay.propTypes = {
  types: PropTypes.array.isRequired,
  pokemonImg: PropTypes.string.isRequired,
  pokemonName: PropTypes.string.isRequired,
}

export default PokemonDisplay

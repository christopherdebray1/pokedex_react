import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import svgr from 'vite-plugin-svgr';

export default defineConfig({
  resolve: {
    alias: {
      '@': '/src',
    },
  },
  plugins: [react({
    jsxImportSource: "@emotion/react",
    babel: {
      plugins: ["@emotion/babel-plugin"],
    },
  }), svgr()],
  test: {
    environment: 'jsdom',
    setupFiles: ['./src/modules/testing/setup.js'],
    testMatch: ['./**/*.test.jsx'],
    globals: true
  }
})

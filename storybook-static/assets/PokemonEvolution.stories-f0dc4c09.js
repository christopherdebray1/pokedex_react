import{j as r,a as n}from"./emotion-react-jsx-runtime.browser.esm-81f7705a.js";import{P as c}from"./index-8d47fad6.js";import{P as j}from"./PokemonDisplay-a75e85b0.js";import{A as D,d as I}from"./Arrow-beba39e9.js";import{c as S}from"./emotion-styled-base.browser.esm-2c30c731.js";import{e as u}from"./pokemon-95931cb3.js";import"./jsx-runtime-ffb262ed.js";import"./index-76fb7be0.js";import"./_commonjsHelpers-de833af9.js";import"./extends-623938b0.js";import"./index-065048de.js";const T=S("div",{target:"e135jidb1"})({name:"s5xdrg",styles:"display:flex;align-items:center"}),_=S("section",{target:"e135jidb0"})("display:flex;flex-direction:",({direction:o})=>{let e=["top","bottom"].includes(o)?"column":"row";return e+=["top","left"].includes(o)?"-reverse":"",e},";",T,"{flex-direction:",({direction:o})=>{let e=["top","bottom"].includes(o)?"column":"row";return e+=["top","left"].includes(o)?"-reverse":"",e},";}p{display:grid;text-align:center;font-weight:bold;font-size:",({theme:o})=>o.size.lg,";}");function q(o){return o.charAt(0).toUpperCase()+o.slice(1)}function A(o){let e=o.toString();for(;e.length<3;)e="0"+e;return e}function m({evolutions:o,pokemonColor:e,direction:p="right"}){return r(_,{direction:p,children:o.map((t,d)=>n(T,{children:[n("div",{children:[r(j,{pokemonImgSize:"sm",isPokemonBackgroundActive:!0,alt:`Image of ${t.name}`,src:t.sprites.other["official-artwork"].front_default}),n("p",{children:[n("span",{children:["#",A(t.id)]}),r("span",{children:q(t.name)})]})]}),d<o.length-1&&r(D,{color:e,direction:p})]},d))})}m.propTypes={evolutions:c.array.isRequired,pokemonColor:c.string.isRequired,direction:c.oneOf(I)};m.__docgenInfo={description:"",methods:[],displayName:"PokemonEvolution",props:{direction:{defaultValue:{value:"'right'",computed:!1},type:{name:"enum",value:[{value:"'top'",computed:!1},{value:"'bottom'",computed:!1},{value:"'left'",computed:!1},{value:"'right'",computed:!1}]},required:!1,description:""},evolutions:{type:{name:"array"},required:!0,description:""},pokemonColor:{type:{name:"string"},required:!0,description:""}}};const J={title:"Pokemon/Pokemon/PokemonEvolution",component:m,tags:["autodocs"],argTypes:{evolutions:{description:"The evolutions of the pokemon, an array of object"},pokemonColor:{description:"The pokemon color, this will define the arrow color"},direction:{description:"The direction of the evolution group display"}}},i={args:{evolutions:u.threeEvolution,pokemonColor:"red"}},s={args:{evolutions:u.twoEvolution,pokemonColor:"yellow"}},l={args:{evolutions:u.noEvolution,pokemonColor:"purple"}},a={args:{evolutions:u.threeEvolution,pokemonColor:"red",direction:"bottom"}};var v,f,g;i.parameters={...i.parameters,docs:{...(v=i.parameters)==null?void 0:v.docs,source:{originalSource:`{
  args: {
    evolutions: evolutions.threeEvolution,
    pokemonColor: 'red'
  }
}`,...(g=(f=i.parameters)==null?void 0:f.docs)==null?void 0:g.source}}};var h,k,E;s.parameters={...s.parameters,docs:{...(h=s.parameters)==null?void 0:h.docs,source:{originalSource:`{
  args: {
    evolutions: evolutions.twoEvolution,
    pokemonColor: 'yellow'
  }
}`,...(E=(k=s.parameters)==null?void 0:k.docs)==null?void 0:E.source}}};var y,P,w;l.parameters={...l.parameters,docs:{...(y=l.parameters)==null?void 0:y.docs,source:{originalSource:`{
  args: {
    evolutions: evolutions.noEvolution,
    pokemonColor: 'purple'
  }
}`,...(w=(P=l.parameters)==null?void 0:P.docs)==null?void 0:w.source}}};var C,x,b;a.parameters={...a.parameters,docs:{...(C=a.parameters)==null?void 0:C.docs,source:{originalSource:`{
  args: {
    evolutions: evolutions.threeEvolution,
    pokemonColor: 'red',
    direction: 'bottom'
  }
}`,...(b=(x=a.parameters)==null?void 0:x.docs)==null?void 0:b.source}}};const K=["Default","TwoEvolution","NoEvolution","DirectionBottom"];export{i as Default,a as DirectionBottom,l as NoEvolution,s as TwoEvolution,K as __namedExportsOrder,J as default};
//# sourceMappingURL=PokemonEvolution.stories-f0dc4c09.js.map

import{A as w}from"./Arrow-beba39e9.js";import"./emotion-react-jsx-runtime.browser.esm-81f7705a.js";import"./jsx-runtime-ffb262ed.js";import"./index-76fb7be0.js";import"./_commonjsHelpers-de833af9.js";import"./extends-623938b0.js";import"./index-8d47fad6.js";import"./emotion-styled-base.browser.esm-2c30c731.js";import"./index-065048de.js";const x={title:"Pokemon/Utility/Arrow",component:w,tags:["autodocs"],argTypes:{color:{description:"The color of the arrows"},direction:{description:"The direction of the arrows"},numberOfArrows:{description:"The number of arrow to display"},routeNavigate:{table:{disable:!0}}}},r={args:{color:"orange"}},o={args:{color:"dodgerBlue"}},e={args:{color:"blue",direction:"top"}},t={args:{color:"red",direction:"left",numberOfArrows:5}};var s,a,c;r.parameters={...r.parameters,docs:{...(s=r.parameters)==null?void 0:s.docs,source:{originalSource:`{
  args: {
    color: 'orange'
  }
}`,...(c=(a=r.parameters)==null?void 0:a.docs)==null?void 0:c.source}}};var n,i,p;o.parameters={...o.parameters,docs:{...(n=o.parameters)==null?void 0:n.docs,source:{originalSource:`{
  args: {
    color: 'dodgerBlue'
  }
}`,...(p=(i=o.parameters)==null?void 0:i.docs)==null?void 0:p.source}}};var l,d,m;e.parameters={...e.parameters,docs:{...(l=e.parameters)==null?void 0:l.docs,source:{originalSource:`{
  args: {
    color: 'blue',
    direction: 'top'
  }
}`,...(m=(d=e.parameters)==null?void 0:d.docs)==null?void 0:m.source}}};var u,g,f;t.parameters={...t.parameters,docs:{...(u=t.parameters)==null?void 0:u.docs,source:{originalSource:`{
  args: {
    color: 'red',
    direction: 'left',
    numberOfArrows: 5
  }
}`,...(f=(g=t.parameters)==null?void 0:g.docs)==null?void 0:f.source}}};const B=["Default","DifferentColor","DirectionTop","MultipleArrow"];export{r as Default,o as DifferentColor,e as DirectionTop,t as MultipleArrow,B as __namedExportsOrder,x as default};
//# sourceMappingURL=Arrow.stories-43afd907.js.map

import{j as r}from"./emotion-react-jsx-runtime.browser.esm-81f7705a.js";import{P as a}from"./PokemonType-4bb3678c.js";import{p as e}from"./pokemon-95931cb3.js";import"./jsx-runtime-ffb262ed.js";import"./index-76fb7be0.js";import"./_commonjsHelpers-de833af9.js";import"./extends-623938b0.js";import"./index-8d47fad6.js";import"./emotion-styled-base.browser.esm-2c30c731.js";const P={title:"Pokemon/Pokemon/PokemonType",component:a,tags:["autodocs"],argTypes:{types:{description:"The types is an array containing one or two object, each object will contain nested object, type within you will find the name key indicating type color",options:[e.charmander.types,e.venusaur.types],control:{type:"radio",labels:{[e.charmander.types]:"Fire",[e.venusaur.types]:"Grass and Poison"}}}}},o=t=>r("div",{style:{display:"flex"},children:r(a,{...t})});o.args={types:e.charmander.types};const s=t=>r("div",{style:{display:"flex"},children:r(a,{...t})});s.args={types:e.venusaur.types};var n,p,i;o.parameters={...o.parameters,docs:{...(n=o.parameters)==null?void 0:n.docs,source:{originalSource:`args => <div style={{
  display: 'flex'
}}>
    <PokemonType {...args} />
  </div>`,...(i=(p=o.parameters)==null?void 0:p.docs)==null?void 0:i.source}}};var d,m,c;s.parameters={...s.parameters,docs:{...(d=s.parameters)==null?void 0:d.docs,source:{originalSource:`args => <div style={{
  display: 'flex'
}}>
    <PokemonType {...args} />
  </div>`,...(c=(m=s.parameters)==null?void 0:m.docs)==null?void 0:c.source}}};o.__docgenInfo={description:"",methods:[],displayName:"Default"};s.__docgenInfo={description:"",methods:[],displayName:"TwoTypes"};const w=["Default","TwoTypes"];export{o as Default,s as TwoTypes,w as __namedExportsOrder,P as default};
//# sourceMappingURL=PokemonType.stories-e39a56b9.js.map

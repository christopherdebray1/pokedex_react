import{j as t}from"./jsx-runtime-ffb262ed.js";import{M as a}from"./index-217a6e60.js";import{P as i}from"./PokemonStats.stories-1dc3f19a.js";import{u as o}from"./index-a1cf9e47.js";import"./index-76fb7be0.js";import"./_commonjsHelpers-de833af9.js";import"./iframe-8bc755db.js";import"../sb-preview/runtime.js";import"./extends-623938b0.js";import"./index-ffc7e5ff.js";import"./index-d37d4223.js";import"./index-356e4a49.js";import"./emotion-react-jsx-runtime.browser.esm-81f7705a.js";import"./index-8d47fad6.js";import"./emotion-styled-base.browser.esm-2c30c731.js";import"./pokemon-95931cb3.js";function s(n){const e=Object.assign({h1:"h1",p:"p",code:"code",h2:"h2",pre:"pre",ul:"ul",li:"li",strong:"strong"},o(),n.components);return t.jsxs(t.Fragment,{children:[t.jsx(a,{of:i}),`
`,t.jsx(e.h1,{id:"pokemonstats-component-documentation",children:"PokemonStats Component Documentation"}),`
`,t.jsxs(e.p,{children:["The ",t.jsx(e.code,{children:"PokemonStats"})," component is a React component used to display the statistical data of a Pokémon. It is designed to render a visual representation of Pokémon stats in a tabular format. This component is typically used in the context of a Pokémon-related application or website."]}),`
`,t.jsx(e.h2,{id:"usage",children:"Usage"}),`
`,t.jsx(e.pre,{children:t.jsx(e.code,{className:"language-javascript",children:`import PokemonStats from './PokemonStats'; // Import the PokemonStats component

<PokemonStats
  stats={/* Array of Pokémon stats data */}
  statRowsCount={/* Number of rows to display */}
  maxStat={/* Maximum stat value */}
/>
`})}),`
`,t.jsx(e.h2,{id:"props",children:"Props"}),`
`,t.jsxs(e.ul,{children:[`
`,t.jsxs(e.li,{children:[t.jsx(e.strong,{children:"stats"})," (Required): An array of Pokémon statistical data. Each item in the array should have the following structure:"]}),`
`]}),`
`,t.jsx(e.pre,{children:t.jsx(e.code,{children:`{
  base_stat: number, // The base value of the stat
  stat: {
    name: string, // The name of the stat (e.g., "HP", "Attack", "Defense", etc.)
  },
}
`})}),`
`,t.jsxs(e.ul,{children:[`
`,t.jsxs(e.li,{children:[`
`,t.jsxs(e.p,{children:[t.jsx(e.strong,{children:"statRowsCount"})," (Optional): The number of rows to display in the table. Default value is 15."]}),`
`]}),`
`,t.jsxs(e.li,{children:[`
`,t.jsxs(e.p,{children:[t.jsx(e.strong,{children:"maxStat"})," (Optional): The maximum possible value for a stat. Default value is 255."]}),`
`]}),`
`]}),`
`,t.jsx(e.h2,{id:"component-structure",children:"Component Structure"}),`
`,t.jsxs(e.p,{children:["The ",t.jsx(e.code,{children:"PokemonStats"})," component is divided into several parts:"]}),`
`,t.jsxs(e.ul,{children:[`
`,t.jsxs(e.li,{children:[`
`,t.jsxs(e.p,{children:[t.jsx(e.strong,{children:"Stat Rows"}),": The statistical data is divided into rows based on the ",t.jsx(e.code,{children:"statRowsCount"})," and ",t.jsx(e.code,{children:"maxStat"})," props. Each row represents a range of stat values. Stat cells within each row are filled or empty based on the comparison with the Pokémon's base stat value."]}),`
`]}),`
`,t.jsxs(e.li,{children:[`
`,t.jsxs(e.p,{children:[t.jsx(e.strong,{children:"Stat Footer"}),': The footer of the table displays the names of the stats. By default, it includes the following stat names: "HP", "Attack", "Defense", "Special Attack (Spe A)", "Special Defense (Spe D)", and "Speed". These names are fixed and do not change based on the ',t.jsx(e.code,{children:"stats"})," prop."]}),`
`]}),`
`]})]})}function w(n={}){const{wrapper:e}=Object.assign({},o(),n.components);return e?t.jsx(e,Object.assign({},n,{children:t.jsx(s,n)})):s(n)}export{w as default};
//# sourceMappingURL=PokemonStats-c46ba3ad.js.map
